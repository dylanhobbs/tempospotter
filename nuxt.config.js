export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'TempoSpotter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto&display=swap'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/auth',
    'vue-scrollto/nuxt',
  ],
  css: ['@/assets/main.scss'],
  env: {
    spotifyId: process.env.SPOTIFY_CLIENT_ID,
    clientUrl: process.env.CLIENT_URL
  },
  router: {
    middleware: ['auth']
  },
  auth: {
    strategies: {
      social: {
        _scheme: 'oauth2',
        authorization_endpoint: 'https://accounts.spotify.com/authorize',
        userinfo_endpoint: 'https://api.spotify.com/v1/me',
        scope: [
          'user-read-private',
          'user-read-email',
          'playlist-read-private',
          'playlist-modify-public',
          'playlist-modify-public'
        ],
        access_type: 'code',
        access_token_endpoint: 'https://accounts.spotify.com/api/token',
        response_type: 'token',
        token_type: 'Bearer',
        redirect_uri: process.env.CALLBACK_URL,
        client_id: 'ef46db29868d4ffd85fb6b87ccd7c235',
        token_key: 'access_token',
        state: 'UNIQUE_AND_NON_GUESSABLE'
      }
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {}
  /*
   ** Build configuration
   */
  // build: {
  //   watch: ['api'],
  //   /*
  //    ** You can extend webpack config here
  //    */
  //   extend(config, ctx) {}
  // },
  // serverMiddleware: ['~/api']
}
