import axios from 'axios'

const clientUrl = process.env.CLIENT_URL
const root = 'https://api.spotify.com/v1'

export const state = () => ({
  formProgress: {
    step: 0,
    searched: false
  },
  playlists: [],
  songResults: [],
  chosenSong: {
    album: {
      images: [{ url: '' }, { url: '' }]
    },
    name: '',
    artists: [{ name: '' }],
    rpm: 0
  },
  chosenSeed: {
    name: '',
    images: [{ url: '' }],
    owner: { display_name: '' }
  },
  bpm: null,
  generatedPlaylist: {},
  spotifyResponse: {
    attempted: false,
    success: false,
    message: ''
  }
})

export const getters = {
  formState: state => {
    return state.formProgress.step
  }
}

export const mutations = {
  updatePlaylists(state, playlists) {
    state.playlists = playlists
  },
  updateSongResults(state, songResults) {
    state.songResults = songResults
  },
  updateChosenSong(state, chosenSong) {
    state.chosenSong = chosenSong
  },
  updateChosenSeed(state, chosenSeed) {
    state.chosenSeed = chosenSeed
  },
  updateBPM(state, bpm) {
    state.bpm = bpm
  },
  updateGeneratedPlaylist(state, generatedPlaylist) {
    state.generatedPlaylist = generatedPlaylist
  },

  setFormSearched(state) {
    state.formProgress.searched = true
  },
  setFromProgress(state, step) {
    state.formProgress.step = step
  },
  setSpotifyResponse(state, response) {
    state.spotifyResponse = response
  }
}
export const actions = {
  /*
    Creates a playlist on Spotify.
    2 calls are invovled here, one to make the playlist and another
    to add the songs to it
  */
  async createPlaylist({ commit, state }, name) {
    let playlist = await this.$axios.$post(
      `${root}/users/${state.auth.user.id}/playlists`,
      {
        name: name,
        description: 'A running playlist tuned for ' + state.auth.user.display_name + '. Made with TempoSpotter',
        public: true
      }
    )

    // Grab a list of song URIs
    let song_uris = state.generatedPlaylist
      .map(song => {
        return song.uri
      })

    // Add songs to the new playlist
    let response = await this.$axios.$post(`${root}/playlists/${playlist.id}/tracks`, {
      uris: song_uris,
    })
    if (response.snapshot_id) {
      commit('setSpotifyResponse', {
        attempted: true,
        success: true,
        message: 'Playlist Created!'
      })
    } else if (response.error) {
      commit('setSpotifyResponse', {
        attempted: true,
        success: false,
        message: `Error ${response.error.status}: ${response.error.message}`
      })  
    } else {
      commit('setSpotifyResponse', {
        attempted: true,
        success: false,
        message: 'An unknown error occurred'
      })  
    }
  },
  /*
    A global form progress value. The stepper form will respect
     any updates to this value
  */
  setProgress: ({ commit, state }, step) => {
    commit('setFromProgress', step)
    return state.formProgress.step
  },
  /*
    Sets a form interaction flag. 
    Can be used to display promts instead of errors
    on empty inputs
  */
  setSearched: ({ commit, state }) => {
    commit('setFormSearched')
    return state.formProgress.searched
  },
  /*
    Perform a search on Spotify. Sets the song results to be 
    the result of a search with passed string.
    Also gets audio features for returned songs and embeds 
    it into the returned product
  */
  async searchSongs({ commit, state }, songName) {
    let songs = await this.$axios.$get(
      `${root}/search?q=${songName}&type=track`
    )
    songs = songs.tracks.items
    // Generate a list of song IDs
    let song_ids = songs
      .map(song => {
        return song.id
      })
      .join(',')

    // Grab the audio features for each result
    let audio_features = await this.$axios.$get(
      `${root}/audio-features?ids=${song_ids}`
    )
    // And modify the song to include 'bpm' key
    // Add any other feature analysis here if you want
    audio_features = audio_features.audio_features
    if (audio_features.length === songs.length) {
      songs.forEach((song, index) => {
        song.bpm = audio_features[index]
          ? Math.round(audio_features[index].tempo)
          : 'unknown'
      })
    }
    commit('updateSongResults', songs)
    return state.songResults
  },
  chooseTrack: ({ commit, state }, track) => {
    commit('updateChosenSong', track)
    commit('updateBPM', track.bpm)
    return state.chosenSong
  },
  async fetchPlaylists({ commit, state }) {
    let playlists = await this.$axios.$get(`${root}/me/playlists?limit=50`)

    commit('updatePlaylists', playlists.items)
    return state.playlists
  },
  chooseSeed: ({ commit, state }, seed) => {
    commit('updateChosenSeed', seed)
    return state.chosenSeed
  },
  setBPM: ({ commit, state }, bpm) => {
    commit('updateBPM', bpm)
    return state.bpm
  },
  async generatePlaylist({ commit, state }) {
    let target_bpm = state.bpm
    // Fetch the playlists tracks
    let playlist_tracks = await this.$axios.$get(
      `${root}/playlists/${state.chosenSeed.id}/tracks?fields=items(track(id))&limit=5`
    )
    // Generate a track list for it
    let seed_tracks = playlist_tracks.items
      .map(song => {
        return song.track.id
      })
      .join(',')

    // Generate reccomendations
    let reccomendations = await this.$axios.$get(
      `${root}/recommendations?limit=20&seed_tracks=${seed_tracks}&target_tempo=${target_bpm}`
    )
    reccomendations = reccomendations.tracks
    // Now get the BPM for each track
    let reccomendations_track_ids = reccomendations
      .map(song => {
        return song.id
      })
      .join(',')
    // Grab the audio features for each result
    let audio_features = await this.$axios.$get(
      `${root}/audio-features?ids=${reccomendations_track_ids}`
    )
    audio_features = audio_features.audio_features
    if (audio_features.length === reccomendations.length) {
      reccomendations.forEach((song, index) => {
        song.bpm = audio_features[index]
          ? Math.round(audio_features[index].tempo)
          : 'unknown'
      })
    }

    commit('updateGeneratedPlaylist', reccomendations)
    return state.generatePlaylist
  }
}
